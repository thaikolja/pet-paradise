import { createObjectCsvWriter } from 'csv-writer'
import { ref }                   from 'vue'

/**
 * Reads the data and saves it into a .csv file
 */
export default defineEventHandler(async (event) => {
  // Takes the data from the body of form
  const body = await readBody(event)

  // Takes the input id (first_name) from the form
  const firstName = body.first_name.trim()

  // Takes the input id (last_name) from the form
  const lastName = body.last_name.trim()

  // Takes the input id (email_address) from the form
  const email = body.email_address.trim()

  // Takes the input id (phone_number) from the form
  const number = body.phone_number.trim()

  // Creating an object for our csvWriter commandline
  const csvWriter = createObjectCsvWriter({
    append: true, // To create a file when needed
    path:   './public/output.csv', // Relative path for the csv file where data is gonna store
    header: [
      {
        id:    'first_name',
        title: 'First Name'
      },
      {
        id:    'last_name',
        title: 'Last Name'
      },
      {
        id:    'email_address',
        title: 'E-Mail'
      },
      {
        id:    'phone_number',
        title: 'Phone Number'
      }
    ]
  })

  // Creating rows where the data will be stored
  const rows = [
    {
      first_name:    firstName,
      last_name:     lastName,
      email_address: email,
      phone_number:  number
    }
  ]

  // shows whether the the data has successfully been added into the record or not
  const defaultOutput = { // OPTIONAL
    status:  '500',
    error:   '',
    message: ''
  }

  // Creating object for status condition
  const output = ref(defaultOutput) // ref is for .vue
  // Side note: let output = defaultOutput

  // Call method that writes the rows in the files.
  await csvWriter.writeRecords(rows)

  // If successful, do this:
  .then(() => {
    output.value.status  = '200' // All good
    output.value.message = 'The form was successfully submitted'
  })
  // catches an error when occurs
  .catch((error) => {
    output.value.status  = '500'
    output.value.error   = error
    output.value.message = 'You have not correctly filled out the form'
  })

  // Returns the output value
  return output.value
})